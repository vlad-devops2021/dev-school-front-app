# Frontend
# устанавливаем самую лёгкую версию JAVA
FROM openjdk:8-jre-alpine
# указываем ярлык. Например, разработчика образа и проч. Необязательный пункт.
LABEL maintainer="zakh.vladislav@yandex.ru"
# указание рабочей директории в контейнере
WORKDIR /app/
# копирование джарника, переименование джарника
COPY devschool-front-app-server/build/libs/devschool-front-app-server-1.0.0.jar frontend.jar
# инфо # внешний порт, по которому наше приложение будет доступно
EXPOSE 8081 
# команда запуска джарника
ENTRYPOINT ["java","-jar","frontend.jar","-port=8081","-P:ktor.backend.port=8080","-P:ktor.backend.host=backend"]
